include:
  - nginx.nginx_install

{%- from "nginx/map.jinja" import server with context %}
{% set default_site_folder = ['/etc/nginx/sites-enabled', '/etc/nginx/sites-available'] %}

{% for folder in default_site_folder  %}
remove {{ folder }}/default:
  file.absent:
    - name: {{ folder }}/default
    - require:
      - pkg: install nginx packages
{% endfor %}

generate upstream.conf:
  file.managed:
    - name: /etc/nginx/upstream.conf
    - source: salt://nginx/files/upstream.conf.j2
    - template: jinja
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot


generate nginx.conf:
  file.managed:
    - name: /etc/nginx/nginx.conf
    - source: salt://nginx/files/nginx.conf.j2
    - template: jinja
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot

{% if server.proxy_header.header_file_path is defined %}
generate proxy header file:
  file.managed:
    - name: {{ server.proxy_header.header_file_path }}
    - source: salt://nginx/files/header.conf.j2
    - template: jinja
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot

{% endif %}

copy Errorpages:
  file.recurse:
    - name: /usr/share/nginx/html/ErrorPages
    - source: salt://nginx/files/ErrorPages
    - require:
      - pkg: install nginx packages
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot

generate dhparams:
  cmd.run:
    - name: openssl dhparam -out /etc/ssl/dhparams.pem 2048
    - creates: /etc/ssl/dhparams.pem
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot

{%- for site_name, site in server.sites.iteritems() %}
{#{%- for site_name, site in server.get('sites', {}).iteritems() %}#}
{%- if site.type == 'nginx_proxy' %}
generate proxy vhost:
  file.managed:
    - name: /etc/nginx/sites-enabled/{{ site.host.name }}.conf
    - source: salt://nginx/files/proxy.conf.j2
    - template: jinja
    - makedirs: True
    - defaults:
        site_name: "{{ site_name }}"
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot

{%- else %}
generate nginx stat:
  file.managed:
    - name: /etc/nginx/sites-enabled/{{ site.host.name }}.conf
    - source: salt://nginx/files/stat.conf.j2
    - template: jinja
    - makedirs: True
    - defaults:
        site_name: "{{ site_name }}"
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot
{%- endif %}
{%- endfor %}

Ensure nginx is started and enabled to start at boot:
  service.running:
    - name: nginx
    - enable: True
    - reload: True
    - require:
      - pkg: install nginx packages
