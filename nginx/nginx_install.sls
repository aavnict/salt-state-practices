{% from "nginx/map.jinja" import server with context %}
install centos epel:
  pkg.installed:
    - name: epel-release

updating all packages after install epel:
  pkg.uptodate:
    - refresh: True

install nginx packages:
  pkg.installed:
    - pkgs: {{ server.pkgs }}
    - require:
      - pkg: install centos epel
