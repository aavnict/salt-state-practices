
install policycoreutils-devel:
  pkg.installed:
    - name: policycoreutils-devel

audit2allow_cmd:
  cmd.run:
    - name: |
        grep nginx /var/log/audit/audit.log | audit2allow -M nginx
    - cwd: /tmp/
    - require:
      - pkg: install policycoreutils-devel

{% if salt['file.file_exists']('/tmp/nginx.pp') %}
semodule_cmd:
  cmd.run:
    - name: |
        semodule -i nginx.pp
    - cwd: /tmp/
    - require:
      - cmd: audit2allow_cmd
{% endif %}
