include:
  - nginx.nginx_configuration
{% if salt['pillar.get']('ssl_certificate:cert:path') and salt['pillar.get']('ssl_certificate:cert:content')%}
create ssl cert:
  file.managed:
    - name: {{ salt['pillar.get']('ssl_certificate:cert:path','/etc/ssl/certs/axi-net.crt') }}
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - contents_pillar: ssl_certificate:cert:content
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot
{% endif %}

{% if salt['pillar.get']('ssl_certificate:key:path') and salt['pillar.get']('ssl_certificate:key:content')%}
create ssl key:
  file.managed:
    - name: {{ salt['pillar.get']('ssl_certificate:key:path','/etc/ssl/private/axi-net.key') }}
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - contents_pillar: ssl_certificate:key:content
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot
{% endif %}

{% if salt['pillar.get']('ssl_certificate:fullchain:path') and salt['pillar.get']('ssl_certificate:fullchain:content')%}
create ssl fullchain:
  file.managed:
    - name: {{ salt['pillar.get']('ssl_certificate:fullchain:path','/etc/ssl/certs/ocsp-chain.crt') }}
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - contents_pillar: ssl_certificate:fullchain:content
    - watch_in:
      - service: Ensure nginx is started and enabled to start at boot
{% endif %}

