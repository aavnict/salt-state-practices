### States description
- Install/Confluguration basic packages, create users, mount partions.
- Install/Configuratin Nginx, Postgresql, Rundeck
- Pillar file in another repositories:
      - https://bitbucket.org/aavnict/salt-pillar-practices/src/master/
      - https://bitbucket.org/aavnict/salt-ict-demo-secrets

### Requirement:
- Install python-gnupg in Master and Minion, it's require by GPG.
- This state tested in Salt master and salt minion version 2018.03
- Minion using Centos 7

### How to run a state

```
     salt '*' state.apply common
	 or
	 salt '*' state.apply
```


### More detail
- See https://axiconsumer.atlassian.net/wiki/spaces/ICT/pages/581107800/KB+SaltStack


