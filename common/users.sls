{% for user, args in pillar['users'].items() %}
create users {{user}}:
  user.present:
    - name: {{ user }}
{% if 'password' in args %}
    - password: {{ args['password'] }}
{% endif %}
{% if 'auth_key' in args %}
add sshkeys user {{ user }}:
  ssh_auth.present:
    - user: {{user}}
    - enc: ssh-rsa
    - options:
      - no-port-forwarding
      - no-agent-forwarding
      - no-X11-forwarding
    - names:
      - {{ args['auth_key'] }}
{% endif %}
{% endfor %}
