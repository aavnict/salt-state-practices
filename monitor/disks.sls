root_device_size:
  disk.status:
    - name: /
    - maximum: '20%'
    - onfail_in:
      - event: alert_admins_disk
alert_admins_disk:
  event.send:
    - name: alert/admins/disk
